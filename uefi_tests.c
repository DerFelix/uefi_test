#include <stdint.h>
#include <efi.h>

// TODO: Check Clang version.
#define va_start(ap, param) __builtin_va_start(ap, param)
#define va_end(ap)          __builtin_va_end(ap)
#define va_arg(ap, type)    __builtin_va_arg(ap, type)
#define va_copy(dest, src)  __builtin_va_copy(dest, src)
typedef __builtin_va_list va_list;


EFI_SYSTEM_TABLE*     gSystemTable = NULL;
EFI_BOOT_SERVICES*    gBootServices = NULL;
EFI_RUNTIME_SERVICES* gRuntimeServices = NULL;


void init_convenience(EFI_HANDLE ImageHandle, EFI_SYSTEM_TABLE* SystemTable) {
  gSystemTable = SystemTable;
  gBootServices = gSystemTable -> BootServices;
  gRuntimeServices = gSystemTable -> RuntimeServices;
}


void __print(CHAR16* text) {
  gSystemTable->ConOut->OutputString(
    gSystemTable->ConOut,
    text
  );
}


void printf(const CHAR16* format, ...) {
  va_list l;
  va_start(l, format);
  
  #define outputbufferlen 20
  #define format_d_len_max 11
  #define format_x_len_max 11
  #define format_default_len_max 3
  CHAR16 outputbuffer[outputbufferlen];
  size_t buf_cursor = 0; // approximates the number of used characters in the outputbuffer
  size_t frm_cursor = 0;
  
  while (format[frm_cursor]) {
    switch (format[frm_cursor]) {
      case '%':
        switch (format[++frm_cursor]) {
          case 'd':
            // Check buffer size
            if (buf_cursor + format_d_len_max >= outputbufferlen) {
              outputbuffer[buf_cursor] = '\0';
              __print(outputbuffer);
              buf_cursor = 0;
            }
            // Obtain variable
            INT32 d = va_arg(l, INT32);
            // Check sign
            if (d < 0) {
              outputbuffer[buf_cursor++] = '-';
              d = -d;
            }
            // Find most significant digit
            INT32 dt = 1000000000; // Note: 9 < log_{10}(2^31) < 10
            while (dt != 0) {
              if (d/dt) {
                break;
              } else {
                dt = dt/10;
              }
            }
            // Print digits (if any).
            outputbuffer[buf_cursor] = '0';
            while (dt != 0) {
              outputbuffer[buf_cursor++] = '0' + (d/dt);
              d = d%dt;
              dt = dt/10;
            }
            // End case
            break;
          case 'x':
            // Check buffer size
            if (buf_cursor + format_d_len_max >= outputbufferlen) {
              outputbuffer[buf_cursor] = '\0';
              __print(outputbuffer);
              buf_cursor = 0;
            }
            // Obtain variable
            UINT32 x = va_arg(l, UINT32);
            // Find most significant digit
            UINT32 xt = 1 << 28;
            while (xt != 0) {
              if (x/xt) {
                break;
              } else {
                xt = xt/0x10;
              }
            }
            // Print digits (if any).
            outputbuffer[buf_cursor++] = '0';
            outputbuffer[buf_cursor++] = 'x';
            if (x!=0) {
              while (xt != 0) {
                if (x/xt < 10) {
                  outputbuffer[buf_cursor++] = '0' + (x/xt);
                } else {
                  outputbuffer[buf_cursor++] = 'A' + (x/xt) - 10;
                }
                x = x%xt;
                xt = xt/0x10;
              }
            } else {
              outputbuffer[buf_cursor++] = '0';
            }
            // End case
            break;
          case 's':
            outputbuffer[buf_cursor] = '\0';
            __print(outputbuffer);
            buf_cursor = 0;
            
            CHAR16* s = va_arg(l, CHAR16*);
            __print(s);
            // End case
            break;
          default:
            // Check buffer size
            if (buf_cursor + format_default_len_max >= outputbufferlen) {
              outputbuffer[buf_cursor] = '\0';
              __print(outputbuffer);
              buf_cursor = 0;
            }
            outputbuffer[buf_cursor++] = '?';
            outputbuffer[buf_cursor++] = '?';
            outputbuffer[buf_cursor++] = '?';
        }
        ++frm_cursor;
        break;
      default:
        if (buf_cursor + 1 >= outputbufferlen) {
          outputbuffer[buf_cursor] = '\0';
          __print(outputbuffer);
          buf_cursor = 0;
        }
        outputbuffer[buf_cursor++] = format[frm_cursor++];
        break;
    }
  }
  outputbuffer[buf_cursor++] = '\0';
  __print(outputbuffer);
  
  
  va_end(l);
}


void print_status() {
  printf(L"Firmware Vendor: ");
  printf(L"%s\r\n", gSystemTable->FirmwareVendor);
  printf(L"Firmware Reversion: %x\r\n", gSystemTable->FirmwareRevision);
  printf(L"UEFI Version: %d.%d\r\n", (gSystemTable->Hdr.Revision) >> 16, (gSystemTable->Hdr.Revision) % (1 << 16));
}


void print_variables() {
  #define bufferlen 100
  CHAR16 variable_name[bufferlen];
  EFI_STATUS status;
  UINTN variable_name_size = bufferlen;
  EFI_GUID vendor_id;
  
  // To start the search, a Null-terminated string is passed in VariableName;
  variable_name[0] = '\0';
  while ((status = gRuntimeServices->GetNextVariableName(&variable_name_size, variable_name, &vendor_id)) == EFI_SUCCESS) {
    printf(L"%s --- ", variable_name);
    variable_name_size = bufferlen;
  }
  
  // Handle errors.
  switch(status) {
    case EFI_NOT_FOUND:
      printf(L"All variables were listed.\r\n");
      break;
    case EFI_BUFFER_TOO_SMALL:
      printf(L"The buffer is too small. We need: %d Bytes.\r\n", variable_name_size);
      break;
    case EFI_INVALID_PARAMETER:
      printf(L"The parameters are invalid.\r\n");
      break;
    case EFI_DEVICE_ERROR:
      printf(L"The variables cannot be retrieved due to a device error.\r\n");
      break;
    default:
      printf(L"An unknown error occured.\r\n");
      break;
  }
}


void call_cpuid(UINT32 function_number, UINT32* val_eax, UINT32* val_ebx, UINT32* val_ecx, UINT32* val_edx) {
  // The instruction cpuid takes a function_number
  // stored in the register eax and stores the
  // value of this function call in the registers
  // eax, ebx, ecx and edx.
  
  __asm__ __volatile__ (
    "mov %4, %%eax\n"
    "cpuid\n"
    "mov %%eax, %0\n"
    "mov %%ebx, %1\n"
    "mov %%ecx, %2\n"
    "mov %%edx, %3\n"
    : "=m" (*val_eax), "=m" (*val_ebx), "=m" (*val_ecx), "=m" (*val_edx)
    : "m" (function_number)
    : "%eax", "%ebx", "%ecx", "%edx"
  );
}

#define CPUIDMaximumExtendedFunctionNumberAndVendorString 0x80000000
#define CPUIDExtendedProcessorAndProcessorFeatureIdentifiers 0x80000001

#define ProcessorFeatureLongMode (1 << 29)

void print_cpu_capabilities() {
  // We assume that the instruction cpuid is available.
  // cpuid takes the value of eax as input.
  // In order to see, wether long mode is available or not,
  // we must call cpuid with eax=0x8000 0001.
  // Then, bit 29 of edx states wether long mode is available or not.
  
  // Calling cpuid with eax > 0x8000 0000 might not correspond to an
  // implemented cpuid function. These functions are 'extended feature
  // functions' The number of the last extended feature function is
  // stored in eax after calling cpuid with eax = 0x8000 0000.
  
  
  UINT32 eax, ebx, ecx, edx;
  // Get last extended feature function
  call_cpuid(CPUIDMaximumExtendedFunctionNumberAndVendorString, &eax, &ebx, &ecx, &edx);
  // Check if we can use the function CPUIDExtendedProcessorAndProcessorFeatureIdentifiers
  if (eax < CPUIDExtendedProcessorAndProcessorFeatureIdentifiers) {
    printf(L"Long Mode unsupported\r\n");
    return;
  }
  
  // Get Processor Features
  call_cpuid(CPUIDExtendedProcessorAndProcessorFeatureIdentifiers, &eax, &ebx, &ecx, &edx);
  // Check if Long Mode is supported
  if (!(edx & ProcessorFeatureLongMode)) {
    printf(L"Long Mode unsupported\r\n");
    return;
  }
  
  printf(L"Long Mode supported\r\n");
}


UINT64 get_model_specific_register(UINT32 register_number) {
  // Model specific registers are retrieved via rdmsr.
  // The register number is provided via ecx.
  // The contents is returned via edx (high bytes) and eax (low bytes).
  UINT64 regh, regl;
  __asm__ __volatile__ (
    "mov %2, %%ecx\n"
    "rdmsr\n"
    "mov %%edx, %0\n"
    "mov %%eax, %1\n"
    : "=m" (regh), "=m" (regl)
    : "r" (register_number)
    : "%eax", "%ecx", "%edx"
  );
  return (((UINT64)regh) << 32) | regl;
}

void set_model_specific_register(UINT32 register_number, UINT64 value) {
  // Model specific registers are written via wrmsr.
  // The register number is provided via ecx.
  // The value is written via edx (high bytes) and eax (low bytes).
  __asm__ __volatile__ (
    "mov %0, %%ecx\n"
    "mov %1, %%edx\n"
    "mov %2, %%eax\n"
    "wrmsr\n"
    :
    : "r" (register_number), "r" ((UINT32)(value>>32)), "r" ((UINT32)value)
    : "%eax", "%ecx", "%edx"
  );
}

#define MSREFER 0xC0000080


void print_controll_registers() {
  UINT32 eferl, cr0l, cr2l, cr3l, cr4l;
  
  // The control register efer is a model specific register.
  eferl = get_model_specific_register(MSREFER);
  
  // The other control registers are retrieved in the usual manner.
  __asm__ __volatile__ (
    "mov %%cr0, %%rax\n"
    "mov %%eax, %0\n"
    "mov %%cr2, %%rax\n"
    "mov %%eax, %1\n"
    "mov %%cr3, %%rax\n"
    "mov %%eax, %2\n"
    "mov %%cr4, %%rax\n"
    "mov %%eax, %3\n"
  : "=m" (cr0l), "=m" (cr2l), "=m" (cr3l), "=m" (cr4l)
  : 
  : "%rax", "%rcx", "%rdx"
  );
  
  printf(L"EFER: %x\r\nCR0:  %x\r\nCR2:  %x\r\nCR3:  %x\r\nCR4:  %x\r\n", eferl, cr0l, cr2l, cr3l, cr4l);
}





EFI_STATUS efi_main(EFI_HANDLE ImageHandle, EFI_SYSTEM_TABLE *SystemTable)
{
  // Initialize global variables in order to use convenient functions
  init_convenience(ImageHandle, SystemTable);
  
  // Set Watchdog Timer to t seconds.
  // The System Firmware is notified after this time elapsed (and might reboot).
  UINT64 t = 0;
  gBootServices->SetWatchdogTimer(t, 0, 0, NULL);
  
  // Print some status messages to the console
  print_status();
  
  // Print the efi variables.
  print_variables();
  
  // Print CPU Capabilities and Control Registers.
  print_cpu_capabilities();
  print_controll_registers();
  
  // Wait for a key press
  EFI_INPUT_KEY Key;
  SystemTable->ConIn->Reset(SystemTable->ConIn, FALSE);
  while ( SystemTable->ConIn->ReadKeyStroke(SystemTable->ConIn, &Key) == EFI_NOT_READY) {
    
  }
 
  return EFI_SUCCESS;
}
