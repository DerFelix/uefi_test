CXX          := clang
CXXFLAGS     := --target=x86_64-unkown-windows -ffreestanding -fshort-wchar -mno-red-zone
INCLUDEPATHS := -I/usr/include/efi -I/usr/include/efi/x86_64
LINKER       := -fuse-ld=lld
LINKERFLAGS  := -nostdlib -Wl,-entry:UefiMain -Wl,-subsystem:efi_application

UEFISRC      := uefi_hello.c
UEFIEXE      := test_uefi_hello_world

DISCFILE     := efi_system_part.img

QEMUBIN      := qemu-system-x86_64
QEMUFLAGS    := -cpu qemu64 -nic none -bios OVMF.fd -display gtk


.PHONY: all, executable, disc, run, clean
.SILENT: clean
all: executable disc run


executable:
	echo "* Building UEFI Application"
	$(CXX) $(CXXFLAGS) $(INCLUDEPATHS) $(LINKER) $(LINKERFLAGS) -o $(UEFIEXE) $(UEFISRC)

disc:
	echo "* Preparing UEFI System Partition"
	dd if=/dev/zero of=$(DISCFILE) bs=1k count=1440 status=none
	mformat -i $(DISCFILE) -f 1440 ::
	echo "* Copying UEFI Application to UEFI System Partition"
	mcopy   -i $(DISCFILE) $(UEFIEXE) ::/

autostart:
	echo "* Copying qemu autostart script to UEFI System Partition"
	echo "FS0:\n"$(UEFIEXE)"\n" > startup.nsh
	mcopy   -i $(DISCFILE) startup.nsh ::/
	rm startup.nsh

run:
	echo "* Starting Emulator"
	$(QEMUBIN) $(QEMUFLAGS) -drive format=raw,file=$(DISCFILE)

clean:
	rm -f $(UEFIEXE)
	rm -f $(DISCFILE)
