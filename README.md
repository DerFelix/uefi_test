# UEFI


System startet Programm (z.B. Bootmanager oder Betriebssystem).
Bedingungen:
- Programm liegt im PE32+ Format vor.
- Programm liegt auf GUID-Partition, die mit efi-system partitioniert ist.


Die Verwendung eines Emulators ist möglich.
Wir verwenden qemu.
qemu startet (wie auch anderen UEFI-fähigen Systeme möglich) in eine UEFI-Shell (mithilfe von OVMF).
Von dieser aus kann das UEFI-Programm gestartet werden.
Benötigten Pakete (unter Debian):
- qemu-system-x86
- ovmf


Wir verwenden die GNU-EFI-Toolchain um ein UEFI Programm zu bauen.
Im Gegensatz zu clang verwendet gcc für Benutzerfunktionen und UEFI-Funktionen verschiedene Calling Convention, weshalb dort UEFI-Funktionen mithilfe des Wrappers `uefi_call_wrapper` aufgerufen werden.
Wir verwenden clang statt gcc.
Benötigte Pakete (unter Debian):
- gnu-efi
- clang
- lld


Bei mir implementiert die GNU-EFI-Toolchain die UEFI-Spezifikation 1.02
Aus /usr/include/efi/efiapi.h:
- #define EFI_SPECIFICATION_MAJOR_REVISION 1
- #define EFI_SPECIFICATION_MINOR_REVISION 02


Um automatisch eine efi-system Partition anzulegen und auf dieser unser Programm abzulegen benötigen wir die folgenden Pakete (unter Debian):
- make
- mtools


Annahme: Wir nutzen ein x86-64 System (das sich zur Ausführung im long-mode befindet)


Einstiegspunkt des UEFI-Programms:
- EFI_STATUS efi_main(EFI_HANDLE ImageHandle, EFI_SYSTEM_TABLE *SystemTable)
  - ImageHandle ist ein Handle, der auf das geladene Abbild zeigt.
    Informationen über das geladene Abbild können mithilfe des Protokolls EFI_LOADED_IMAGE_PROTOCOL angefordert werden.
  - SystemTable enthält Zeiger auf Tabellen die die verwendbaren Laufzeit- und Bootservices bereitstellen.


Die funktionsweise von "Variable Argumentlisten" werden durch den C-Standard definiert.
Die Umsetzung (i.e. die Implementierung von va_xxx) geschieht üblicherweise durch den Compliler.
So wird z.B. von clang in stdarg.h definiert:
- #define va_start(ap, param) __builtin_va_start(ap, param)
- #define va_end(ap)          __builtin_va_end(ap)
- #define va_arg(ap, type)    __builtin_va_arg(ap, type)
- #define va_copy(dest, src)  __builtin_va_copy(dest, src)


Zugang über EFI Tables:
- EFI_SYSTEM_TABLE
  - After an operating system has taken control of the platform with a call to ExitBootServices(),
    only the Hdr, FirmwareVendor, FirmwareRevision, RuntimeServices, NumberOfTableEntries, and ConfigurationTable
    fields are valid.
- EFI_BOOT_SERVICES
  - The function pointers in this table are not valid after the operating system has taken control
    of the platform with a call to EFI_BOOT_SERVICES.ExitBootServices().
- EFI_RUNTIME_SERVICES
  - Unlike the EFI Boot Services Table, this table, and the function pointers it contains are valid after the
    UEFI OS loader and OS have taken control of the platform with a call to EFI_BOOT_SERVICES.ExitBootServices().
    If a call to SetVirtualAddressMap() is made by the OS, then the function pointers in this table are fixed
    up to point to the new virtually mapped entry points.



# AMD64
Virtuelle Adressen müssen in `Canonical Form` sein, d.h. alle nicht verwendbaren Adressbits müssen entweder alle 0 oder alle 1 sein, siehe [AMD2 1.1.3]

Paging: Translation tables are aligned on 4-Kbyte boundaries. Physical pages must be aligned on 4-Kbyte, 2-Mbyte, or 4-Mbyte boundaries, depending on the physical-page size. [AMD2 1.2.2]

Neben Long Mode und Legacy Mode gibt es noch den System Management Mode. Dieser wird bei einem System Management Interrupt aktiviert. [AMD2 1.3.5]

Eine Liste der System Register findet sich in [AMD2 1.4].

Beim Start: Paging mode is enabled and any memory space defined by the UEFI memory map is identity mapped (virtual address equals physical address), although the attributes of certain regions may not have all read, write, and execute attributes or be unmarkedfor purposes of platform protection. The mappings to other regions are undefined and may vary from implementation to implementation. [AMD 2.3.4]

Entering Long Mode:
- The AMD64 architecture requires physical-address extensions to be enabled (CR4.PAE=1) before long mode is entered [AMD2 2.2.2].
- 

There are two method for automatically switch stack frames in response to an interrupt.
- Long-Mode Stack Switching
- Interrupt Stack Tables


# Linux
See
- https://lwn.net/Articles/632528/
- linuxsrc/arch/x86/boot/compressed/eboot.c

The kernel can be (and is most probably) build as efi executable.
You can use `readpe /boot/vmlinuz` and `pedis -e /boot/vmlinuz`.
The entry point is (on my system) the function `efi_pe_entry` defined in `src_base/arch/x86/boot/compressed/head_64.S`.
Beware: We do see this only as entry point; the calling convention for the entry point are via `MS x64`, however, all other calls are via `System V` and not `MS x64`.

