#include <stdint.h>
#include <efi.h>

EFI_STATUS UefiMain(EFI_HANDLE ImageHandle, EFI_SYSTEM_TABLE *SystemTable)
{
  
  SystemTable->ConOut->OutputString(SystemTable->ConOut, L"Hello World!\n");
  
  // Wait for a key press
  EFI_INPUT_KEY Key;
  SystemTable->ConIn->Reset(SystemTable->ConIn, FALSE);
  while ( SystemTable->ConIn->ReadKeyStroke(SystemTable->ConIn, &Key) == EFI_NOT_READY) {
    
  }
 
  return EFI_SUCCESS;
}
